class AssetsPath {
  static const String imgFood1 = 'assets/img/imgFood1.jpeg';
  static const String imgAvater = 'assets/img/img1.jpg';
  static const String imgFood2 = 'assets/img/imgFood2.jpg';
  static const String imgWelcome = 'assets/img/start up page.jpg';
  static const String imgLogo = 'assets/img/logoreview.png';

  static const String img3 = 'assets/img/img3.jpg';
  static const String iconReviews = 'assets/icons/reviews.svg';
  static const String iconSearch = 'assets/icons/search.svg';
  static const String iconPen = 'assets/icons/pen.svg';
  static const String iconNotifications = 'assets/icons/notification.svg';
  static const String iconProfile = 'assets/icons/profile.svg';
  static const String iconDotshHorizontal = 'assets/icons/dots-horizontal.svg';
  static const String iconTDownHS = 'assets/icons/thumbs-down-silhouette.svg';
  static const String iconTUpHS = 'assets/icons/thumbs-up-hand-symbol.svg';
  static const String iconRedo = 'assets/icons/redo.svg';
  static const String iconChat = 'assets/icons/Chat.svg';
  static const String iconMenu = 'assets/icons/menu.svg';
  static const String iconAddLocation = 'assets/icons/maps-and-flags.svg';
  static const String iconAddPhotos = 'assets/icons/image.svg';
  static const String iconAddVideos = 'assets/icons/video-camera.svg';
  static const String iconGoogle = 'assets/icons/google.svg';
  static const String iconFacebook = 'assets/icons/facebook.svg';
}
