
import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/constants/assets_path.dart';

class AppAnimatedLogo extends StatelessWidget {
  const AppAnimatedLogo({
    Key? key,
    required this.size,
  }) : super(key: key);

  final double size;

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      curve: Curves.bounceInOut,
      duration: const Duration(milliseconds: 2000),
      width: size,
      height: size,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.5),
            spreadRadius: 8,
            blurRadius: 13,
          ),
        ],
        color: Colors.white,
        borderRadius: BorderRadius.circular(100),
        image: const DecorationImage(
          image: AssetImage(AssetsPath.imgLogo),
        ),
      ),
    );
  }
}
