import 'package:flutter/material.dart';

import '../config/themes/app_color.dart';
import '../constants/assets_path.dart';
import 'app_body_posts.dart';
import 'app_circle_avatar.dart';
import 'app_icon.dart';
import 'app_text_sub_title.dart';
import 'app_text_title.dart';

class AppPosts extends StatelessWidget {
  const AppPosts({
    Key? key,
    required this.widget,
    required this.index,
  }) : super(key: key);
  final Widget widget;
  final int index;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 20, right: 15, left: 15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: AppColors.cardWhite,
      ),
      child: Column(children: [
        InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => widget,
              ),
            );
          },
          child: Container(
            padding: const EdgeInsets.only(top: 20, right: 20, left: 20),
            child: Row(
              children: [
                AppCircleAvatar(
                  img: AssetsPath.imgAvater,
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      AppTextTitle(
                        text: 'Carolyne Agustine',
                        sizeText: 20,
                        color: AppColors.textColorDarkBlue,
                      ),
                      AppTextSubTitle(
                        text: '26 mintes ago',
                        color: AppColors.textColorGray91,
                      )
                    ],
                  ),
                ),
                const AppIcon(assetIcon: AssetsPath.iconDotshHorizontal)
              ],
            ),
          ),
        ),
        const SizedBox(height: 20),
        Container(
          width: double.maxFinite,
          height: 1,
          color: AppColors.cardLightGray,
        ),
        Container(
          padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
          child: AppBoydyPosts(
            isImg: index == 2 ? false : true,
          ),
        )
      ]),
    );
  }
}
