import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/widgets/app_text_sub_title.dart';
import '../../constants/assets_path.dart';
import 'app_circle_avatar.dart';
import 'app_text_title.dart';

class AppHeader extends StatelessWidget {
  const AppHeader({
    Key? key,
    this.assetIcon = AssetsPath.iconMenu,
    this.isIcon = true,
    this.isSubtitle = true,
    required this.widget,
    required this.textTitle,
    this.textSubTitle = '',
    required this.bodyWidget,
    required this.press,
    this.isIconBack = false,
    this.textTitleSize = 30,
  }) : super(key: key);
  final String assetIcon;
  final String textTitle;
  final String textSubTitle;
  final double textTitleSize;
  final bool isIcon;
  final bool isIconBack;
  final bool isSubtitle;
  final Widget widget;
  final Widget bodyWidget;
  final VoidCallback press;
  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.kPrimaryColor,
      padding: const EdgeInsets.only(top: 50),
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.only(top: 20, right: 20, bottom: 20),
            child: Row(
              children: [
                Visibility(
                  visible: isIconBack,
                  child: Row(
                    children: [
                      IconButton(
                        onPressed: press,
                        icon: const Icon(
                          Icons.chevron_left,
                          color: Colors.white,
                        ),
                      ),
                      AppCircleAvatar(
                        img: AssetsPath.imgAvater,
                      ),
                    ],
                  ),
                ),
                const SizedBox(width: 20),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AppTextTitle(
                      text: textTitle,
                      sizeText: textTitleSize,
                    ),
                    Visibility(
                      visible: isSubtitle,
                      child: AppTextSubTitle(text: textSubTitle),
                    ),
                  ],
                ),
                const Spacer(
                  flex: 1,
                ),
                isIcon == true
                    ? SizedBox(
                        width: 24,
                        height: 24,
                        child: SvgPicture.asset(
                          assetIcon,
                          color: AppColors.iconGainsboro,
                        ),
                      )
                    : widget,
              ],
            ),
          ),
          bodyWidget
        ],
      ),
    );
  }
}
