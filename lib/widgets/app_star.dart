import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/widgets/app_text_sub_title.dart';

class AppStar extends StatefulWidget {
  AppStar({
    Key? key,
    required this.status,
    this.isOnPressed = false,
  }) : super(key: key);
  late int status;
  bool isOnPressed;

  @override
  State<AppStar> createState() => _AppStarState();
}

class _AppStarState extends State<AppStar> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Align(
            alignment:
                widget.isOnPressed ? Alignment.centerLeft : Alignment.center,
            child: Wrap(
              children: List.generate(
                5,
                (index) => IconButton(
                  icon: Icon(Icons.star,
                      size: 40,
                      color: index < widget.status
                          ? AppColors.starColorYellow
                          : AppColors.starColorLightYellow),
                  onPressed: widget.isOnPressed
                      ? () {
                          setState(() {
                            widget.status = index + 1;
                          });
                        }
                      : () {},
                ),
              ),
            ),
          ),
        ),
        Visibility(
          visible: widget.isOnPressed,
          child: widget.status == 5
              ? const AppTextSubTitle(
                  text: 'Wow!',
                )
              : const Text(''),
        )
      ],
    );
  }
}
