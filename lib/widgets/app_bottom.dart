import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';

import 'app_text_sub_title.dart';

class AppBottom extends StatelessWidget {
  const AppBottom({
    Key? key,
    required this.text,
    this.sizeText = 25,
     this.radius=10,
    this.colorButtom = AppColors.buttomColorGray56,
    this.colorText = AppColors.textColorWhite,
  }) : super(key: key);
  final String text;
  final double sizeText;
  final double radius;
  final Color colorButtom;
  final Color colorText;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: 50,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(radius),
        color: colorButtom,
      ),
      child: Center(
        child: AppTextSubTitle(
          text: text,
          sizeText: sizeText,
          color: colorText,
        ),
      ),
    );
  }
}
