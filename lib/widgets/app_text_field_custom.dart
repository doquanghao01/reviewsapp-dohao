import 'package:flutter/material.dart';

class AppTextFieldCustom extends StatelessWidget {
  const AppTextFieldCustom({
    Key? key,
    required this.hintText,
    this.textSize = 18,
    this.height = 50,
    this.isObscureText = false,
    this.icon = Icons.abc,  this.isIcon=false,
  }) : super(key: key);
  final String hintText;
  final double textSize;
  final double height;
  final bool isObscureText;
  final bool isIcon;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20),
      padding: const EdgeInsets.only(right: 20, left: 20),
      width: MediaQuery.of(context).size.width * 1,
      height: height,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Center(
        child: TextField(
          obscureText: isObscureText,
          style: TextStyle(fontSize: textSize),
          decoration: InputDecoration(
              icon: Visibility(
                visible: isIcon,
                child: Icon(
                  icon,
                  color: const Color.fromARGB(255, 126, 126, 126),
                ),
              ),
              hintText: hintText,
              border: InputBorder.none,
              hintStyle: TextStyle(
                fontSize: textSize,
              )),
        ),
      ),
    );
  }
}
