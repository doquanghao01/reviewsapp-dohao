import 'package:flutter/cupertino.dart';
import '../config/themes/app_color.dart';

class AppTextSubTitle extends StatelessWidget {
  const AppTextSubTitle({
    Key? key,
    required this.text,
    this.sizeText = 20,
    this.color = AppColors.textColorGray91,
    this.isTextAlign = false,
    this.maxLines = 1,
    this.isOverflow = false,
    this.isMaxLines = false,
  }) : super(key: key);
  final String text;
  final double sizeText;
  final Color color;
  final int maxLines;
  final bool isOverflow;
  final bool isTextAlign;
  final bool isMaxLines;
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: isTextAlign ? TextAlign.center : TextAlign.start,
      overflow: isOverflow ? TextOverflow.ellipsis : TextOverflow.fade,
      maxLines: isMaxLines ? maxLines : null,
      style: TextStyle(fontSize: sizeText, color: color),
    );
  }
}
