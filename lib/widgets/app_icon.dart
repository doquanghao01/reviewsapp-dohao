import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/widgets/app_text_sub_title.dart';

class AppIcon extends StatelessWidget {
  const AppIcon(
      {Key? key,
      required this.assetIcon,
      this.text = '',
      this.isPress = false,
      this.sizeIcon = 24,
      this.isText = true})
      : super(key: key);
  final String assetIcon;
  final String text;
  final bool isPress;
  final bool isText;
  final double sizeIcon;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(
          width: sizeIcon,
          height: sizeIcon,
          child: SvgPicture.asset(
            assetIcon,
            color: isPress == true ? AppColors.iconBlue : AppColors.iconGray,
          ),
        ),
        const SizedBox(
          height: 5,
        ),
        Visibility(
          visible: isText,
          child: AppTextSubTitle(
            text: text,
            sizeText: 15,
            isTextAlign: true,
            color: isPress == true ? AppColors.iconBlue : AppColors.iconGray,
          ),
        )
      ],
    );
  }
}
