import 'package:flutter/cupertino.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../config/themes/app_color.dart';

class AppTextTitle extends StatelessWidget {
  const AppTextTitle(
      {Key? key,
      required this.text,
      this.sizeText = 32,
      this.color = AppColors.textColorWhite})
      : super(key: key);
  final String text;
  final double sizeText;
  final Color color;
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
          fontWeight: FontWeight.bold, fontSize: sizeText, color: color),
    );
  }
}
