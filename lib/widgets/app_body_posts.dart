import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/constants/assets_path.dart';
import 'package:reviewsapp_haodo/widgets/app_text_sub_title.dart';
import 'package:reviewsapp_haodo/widgets/app_text_title.dart';

import 'app_icon.dart';
import 'app_star.dart';

class AppBoydyPosts extends StatelessWidget {
  const AppBoydyPosts({
    Key? key,
    this.isImg = true,
  }) : super(key: key);
  final bool isImg;

  @override
  Widget build(BuildContext context) {
    String text =
        'I\'m very happy with the services. I think this is the best cafe in Yogyakar';
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 15, bottom: 15),
          child: const AppTextTitle(
            text: 'Secangkir jawa',
            color: AppColors.textColorDarkBlue,
          ),
        ),
        AppStar(status: 5),
        Container(
          margin: const EdgeInsets.only(top: 15, bottom: 15),
          child: Text.rich(
            TextSpan(
              children: [
                TextSpan(
                    text:
                        text.length > 74 ? "${text.substring(0, 74)}..." : text,
                    style: const TextStyle(
                      fontSize: 20,
                      color: AppColors.textColorDarkBlue,
                    )),
                const TextSpan(
                    text: 'See More',
                    style: TextStyle(
                      fontSize: 20,
                      color: AppColors.textColorGray91,
                    )),
              ],
            ),
          ),
        ),
        Visibility(
          visible: isImg,
          child: const ImgPosts(),
        ),
        const CateagoryAndLocationPosts(),
        Container(
          padding: const EdgeInsets.only(left: 30, right: 30),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              AppIcon(
                assetIcon: AssetsPath.iconTUpHS,
                text: '68',
              ),
              AppIcon(
                assetIcon: AssetsPath.iconTDownHS,
                text: '0',
              ),
              AppIcon(
                assetIcon: AssetsPath.iconChat,
                text: '8',
              ),
              AppIcon(
                assetIcon: AssetsPath.iconRedo,
                text: '3',
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class CateagoryAndLocationPosts extends StatelessWidget {
  const CateagoryAndLocationPosts({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10, bottom: 10),
      padding: const EdgeInsets.all(15),
      width: double.maxFinite,
      decoration: BoxDecoration(
        color: AppColors.cardLightGray,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              AppTextTitle(
                text: 'Cartegory',
                sizeText: 20,
                color: AppColors.textColorDarkBlue,
              ),
              SizedBox(height: 5),
              AppTextSubTitle(
                text: 'Food & Drink',
                color: AppColors.textColorGray91,
              )
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              AppTextTitle(
                text: 'Location',
                sizeText: 20,
                color: AppColors.textColorDarkBlue,
              ),
              SizedBox(height: 5),
              AppTextSubTitle(
                text: 'Seacang Yogyakarta',
                color: AppColors.textColorGray91,
              )
            ],
          )
        ],
      ),
    );
  }
}

class ImgPosts extends StatelessWidget {
  const ImgPosts({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          width: 190,
          height: 120,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            image: const DecorationImage(
                image: AssetImage(AssetsPath.imgFood2), fit: BoxFit.cover),
          ),
        ),
        Container(
          width: 120,
          height: 120,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            image: const DecorationImage(
                image: AssetImage(AssetsPath.imgFood1), fit: BoxFit.cover),
          ),
          child: Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.black.withOpacity(0.3),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                AppTextTitle(text: '6'),
                AppTextTitle(text: 'more'),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
