import 'package:flutter/painting.dart';

class AppColors {
  static const kPrimaryColor = Color(0xFF402FB4);
  static const kButtomNavBarColor = Color(0xFFFFFFFF);
  static const kIconOffColor = Color(0xFF98A0B0);

  static const Color mainColorsWhite = Color(0xFFFFFFFF);

  static const Color textColorWhite = Color(0xFFFFFFFF);
  static const Color textColorDarkBlue = Color(0xFF233757);
  static const Color textColorGray91 = Color(0xFFB8BDC6);
  static const Color textHintColorGray91 = Color(0xFFB8BDC6);
  static const Color textColorpurple54 = Color(0xFF5C4EAE);

  static const Color cardWhite = Color(0xFFFFFFFF);
  static const Color cardLightGray = Color(0xFFF2F2F2);

  static const Color iconGray = Color(0xFF98A0B0);
  static const Color iconBlue = Color(0xFF402FB4);
  static const Color iconGainsboro = Color(0xFFA699FF);

  static const Color starColorYellow = Color(0xFFFFAD53);

  static const Color starColorLightYellow = Color(0xFFE1E1E1);

  static const Color bottomNaviColor = Color(0xFFFFFFFF);

  static const Color buttomColorGray91 = Color(0xFFF9F9F9);
  static const Color buttomColorGray56 = Color(0xFFD8DBE3);
  static const Color dottedBorderColorGray82 = Color(0xFFE2E2E2);

//   static const Color textColorDarkBlue = Color(0xFF233757);

//   static const Color textColorDarkBlue = Color(0xFF233757);

}
