import 'dart:io';

import 'package:chewie/chewie.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/constants/assets_path.dart';
import 'package:reviewsapp_haodo/widgets/app_text_title.dart';
import 'package:video_player/video_player.dart';

import 'buttom_dotted_border.dart';

class AddVideo extends StatefulWidget {
  const AddVideo({Key? key}) : super(key: key);

  @override
  State<AddVideo> createState() => _AddVideoState();
}

class _AddVideoState extends State<AddVideo> {
  VideoPlayerController? _controller;
  getVideo() async {
    final XFile? image =
        await ImagePicker().pickVideo(source: ImageSource.gallery);
    setState(() {
      if (image != null) {
        _controller = VideoPlayerController.file(File(image.path))
          ..initialize().then((_) {
            setState(() {});
          });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Visibility(
          visible: _controller == null ? true : false,
          child: ButtomDottedBorder(
            assetIcon: AssetsPath.iconAddVideos,
            textContent: 'Click to add video',
            textTitle: 'Upload Videos',
            press: () {
              getVideo();
            },
          ),
        ),
        if (_controller != null)
          Visibility(
            visible: _controller!.value.isInitialized ? true : false,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                const Align(
                  alignment: Alignment.topLeft,
                  child: AppTextTitle(
                    text: 'Video',
                    sizeText: 26,
                    color: AppColors.textColorDarkBlue,
                  ),
                ),
                InkWell(
                  onTap: () {
                    setState(() {
                      _controller = null;
                    });
                  },
                  child: const Icon(
                    Icons.clear,
                    color: Colors.red,
                  ),
                ),
                const SizedBox(height: 10),
                AspectRatio(
                  aspectRatio: 4 / 2,
                  child: Chewie(
                    controller: ChewieController(
                      videoPlayerController: _controller!,
                      autoPlay: false,
                      looping: false,
                    ),
                  ),
                ),
              ],
            ),
          ),
        const SizedBox(height: 20),
      ],
    );
  }
}
