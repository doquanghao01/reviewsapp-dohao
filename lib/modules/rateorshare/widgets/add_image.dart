import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/constants/assets_path.dart';
import 'package:reviewsapp_haodo/modules/rateorshare/widgets/buttom_dotted_border.dart';
import 'package:reviewsapp_haodo/widgets/app_text_title.dart';

class AddImage extends StatefulWidget {
  const AddImage({Key? key}) : super(key: key);

  @override
  State<AddImage> createState() => _AddImageState();
}

class _AddImageState extends State<AddImage> {
  List listImg = [];

  Widget getImageWidget(int index) {
    if (listImg.isNotEmpty) {
      return Container(
        width: 100,
        height: 100,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          image: DecorationImage(
            image: FileImage(File(listImg[index]!.path)),
            fit: BoxFit.cover,
          ),
        ),
      );
    } else {
      return Container(
        width: 100,
        height: 100,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          image: const DecorationImage(
            image: AssetImage(AssetsPath.imgFood1),
            fit: BoxFit.cover,
          ),
        ),
      );
    }
  }

  getImage(ImageSource source) async {
    XFile? imageFile = await ImagePicker().pickImage(source: source);
    if (imageFile != null) {
      CroppedFile? croppedFile = await ImageCropper().cropImage(
        sourcePath: imageFile.path,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
        ],
        uiSettings: [
          AndroidUiSettings(
              toolbarTitle: 'Cropper',
              toolbarColor: AppColors.kPrimaryColor,
              toolbarWidgetColor: Colors.white,
              initAspectRatio: CropAspectRatioPreset.original,
              lockAspectRatio: false),
          IOSUiSettings(
            title: 'Cropper',
          ),
        ],
      );
      setState(
        () {
          if (croppedFile != null) {
            listImg.add(XFile(croppedFile.path));
          }
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        addImage(context),
        Visibility(
          visible: listImg.length > 5 ? false : true,
          child: ButtomDottedBorder(
            assetIcon: AssetsPath.iconAddPhotos,
            textContent: 'Click to add photo',
            textTitle: 'Upload Photos',
            press: () {
              getImage(ImageSource.gallery);
            },
          ),
        ),
      ],
    );
  }

  Visibility addImage(BuildContext context) {
    return Visibility(
      visible: listImg.isEmpty ? false : true,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const AppTextTitle(
            text: 'Image',
            sizeText: 26,
            color: AppColors.textColorDarkBlue,
          ),
          const SizedBox(height: 10),
          Container(
            width: MediaQuery.of(context).size.width * 1,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: AppColors.cardLightGray),
            child: Wrap(
              children: List.generate(
                listImg.length,
                (index) => Padding(
                  padding: const EdgeInsets.all(10),
                  child: getImageWidget(index),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
