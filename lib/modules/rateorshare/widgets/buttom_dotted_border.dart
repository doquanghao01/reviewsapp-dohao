import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/widgets/app_icon.dart';
import 'package:reviewsapp_haodo/widgets/app_text_title.dart';

class ButtomDottedBorder extends StatelessWidget {
  final String assetIcon;
  final String textTitle;
  final String textContent;
  final double sizeHeight;
  final VoidCallback press;
  const ButtomDottedBorder({
    Key? key,
    required this.assetIcon,
    required this.textTitle,
    this.sizeHeight = 120,
    required this.textContent,
    required this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: press,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AppTextTitle(
            text: textContent,
            sizeText: 26,
            color: AppColors.textColorDarkBlue,
          ),
          const SizedBox(height: 20),
          DottedBorder(
            borderType: BorderType.RRect,
            color: AppColors.dottedBorderColorGray82,
            radius: const Radius.circular(12),
            dashPattern: const [8, 4],
            strokeWidth: 2,
            child: ClipRRect(
              borderRadius: const BorderRadius.all(Radius.circular(12)),
              child: Container(
                height: sizeHeight,
                width: double.maxFinite,
                color: AppColors.buttomColorGray91,
                child: Container(
                  padding: const EdgeInsets.only(
                      top: 30, bottom: 30, left: 50, right: 50),
                  child: AppIcon(
                    assetIcon: assetIcon,
                    text: textTitle,
                    sizeIcon: 32,
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }
}
