
import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/modules/rateorshare/widgets/add_map.dart';
import 'package:reviewsapp_haodo/widgets/app_bottom.dart';
import 'add_image.dart';
import 'add_video.dart';
import 'cayrgory.dart';
import 'review_text_field.dart';
import 'text_field_comment.dart';
import 'your_rate.dart';

class Body extends StatefulWidget {
  const Body({
    Key? key,
  }) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          padding: const EdgeInsets.all(15),
          decoration: const BoxDecoration(
            color: AppColors.cardWhite,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              ReviewTittle(),
              YourRate(),
              Cayrgory(),
              SizedBox(height: 20),
              AddMap(),
               TextFieldComment(),
              SizedBox(height: 20),
              AddImage(),
              AddVideo(),
              AppBottom(
                colorButtom: AppColors.kPrimaryColor,
                text: 'Submit Your Review',
                
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }


}
