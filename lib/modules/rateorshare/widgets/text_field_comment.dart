import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/widgets/app_text_title.dart';

class TextFieldComment extends StatelessWidget {
  const TextFieldComment({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const AppTextTitle(
          text: 'Your Comment',
          sizeText: 26,
          color: AppColors.textColorDarkBlue,
        ),
        const SizedBox(height: 20),
        Container(
          padding: const EdgeInsets.all(20),
          height: 160,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
                color: AppColors.dottedBorderColorGray82, width: 1.0),
          ),
          child: const TextField(
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
            maxLines: null,
            minLines: 1,
            decoration: InputDecoration(
              hintText: "Write your comment here...",
              hintStyle: TextStyle(
                color: AppColors.textColorGray91,
                fontSize: 20,
              ),
              border: InputBorder.none,
            ),
          ),
        ),
      ],
    );
  }
}
