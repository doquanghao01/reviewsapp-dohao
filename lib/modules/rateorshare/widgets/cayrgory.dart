import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/widgets/app_text_sub_title.dart';
import 'package:reviewsapp_haodo/widgets/app_text_title.dart';

class Cayrgory extends StatelessWidget {
  const Cayrgory({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<String> genderItems = ['Drinks', 'Cake', 'Foods'];
    String? selectedValue;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 20),
        const AppTextTitle(
          text: 'Cayrgory',
          sizeText: 26,
          color: AppColors.textColorDarkBlue,
        ),
        const SizedBox(height: 30),
        DropdownButtonFormField2(
          decoration: InputDecoration(
            isDense: true,
            contentPadding: EdgeInsets.zero,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
            ),
          ),
          hint: const AppTextSubTitle(
            text: 'Select Category',
            color: AppColors.textColorDarkBlue,
            sizeText: 25,
          ),
          icon: const Icon(
            Icons.keyboard_arrow_down_rounded,
            color: Colors.black45,
          ),
          iconSize: 30,
          buttonHeight: 50,
          buttonPadding: const EdgeInsets.only(left: 30, right: 10),
          dropdownDecoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
          ),
          items: genderItems
              .map((item) => DropdownMenuItem<String>(
                    value: item,
                    child: Text(
                      item,
                      style: const TextStyle(fontSize: 25, color: Colors.black),
                    ),
                  ))
              .toList(),
          validator: (value) {
            if (value == null) {
              return 'you don\'t have data';
            }
          },
          onChanged: (value) {},
          onSaved: (value) {
            selectedValue = value.toString();
          },
        )
      ],
    );
  }
}
