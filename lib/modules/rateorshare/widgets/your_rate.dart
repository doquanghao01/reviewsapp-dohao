import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/widgets/app_star.dart';
import 'package:reviewsapp_haodo/widgets/app_text_title.dart';

class YourRate extends StatefulWidget {
  const YourRate({
    Key? key,
  }) : super(key: key);

  @override
  State<YourRate> createState() => _YourRateState();
}

class _YourRateState extends State<YourRate> {
  int status = 0;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const AppTextTitle(
          text: 'Whats\' Your Rate?',
          sizeText: 26,
          color: AppColors.textColorDarkBlue,
        ),
        const SizedBox(height: 20),
        AppStar(
          status: 0,
          isOnPressed: true,
        ),
      ],
    );
  }
}
