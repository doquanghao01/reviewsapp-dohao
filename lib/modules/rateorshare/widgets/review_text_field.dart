import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/widgets/app_text_title.dart';

class ReviewTittle extends StatelessWidget {
  const ReviewTittle({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: const [
        AppTextTitle(
          text: 'Review Tile',
          color: AppColors.textColorDarkBlue,
          sizeText: 26,
        ),
        TextField(
          style: TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.bold,
          ),
          decoration: InputDecoration(
            hintText: "Enter the Title",
            hintStyle: TextStyle(
              fontSize: 30.0,
              color: AppColors.textHintColorGray91,
            ),
          ),
        ),
        SizedBox(height: 20),
      ],
    );
  }
}
