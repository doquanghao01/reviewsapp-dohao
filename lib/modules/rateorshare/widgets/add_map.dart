import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geocode/geocode.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/constants/assets_path.dart';
import 'dart:async';

import 'package:reviewsapp_haodo/modules/rateorshare/widgets/buttom_dotted_border.dart';
import 'package:reviewsapp_haodo/widgets/app_text_sub_title.dart';
import 'package:reviewsapp_haodo/widgets/app_text_title.dart';

class AddMap extends StatefulWidget {
  const AddMap({Key? key}) : super(key: key);

  @override
  State<AddMap> createState() => _AddMapState();
}

class _AddMapState extends State<AddMap> {
  String addressc = '';
  late LatLng currentLatLng = const LatLng(9, 0);
  final Completer<GoogleMapController> _controller = Completer();
  Future<void> determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    Address address = await GeoCode().reverseGeocoding(
        latitude: position.latitude, longitude: position.longitude);
    setState(() {
      currentLatLng = LatLng(position.latitude, position.longitude);
      addressc =
          '${address.streetAddress}, ${address.city}, ${address.countryName}';
    });
  }

  Future<void> goToCurrentLocation() async {
    await determinePosition();
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: currentLatLng, zoom: 14)));
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Visibility(
          visible: addressc.isNotEmpty ? true : false,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const AppTextTitle(
                text: 'Location',
                sizeText: 26,
                color: AppColors.textColorDarkBlue,
              ),
              const SizedBox(height: 20),
              Container(
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(20)),
                width: double.maxFinite,
                height: 200,
                child: GoogleMap(
                  mapType: MapType.normal,
                  initialCameraPosition:
                      CameraPosition(target: currentLatLng, zoom: 14),
                  onMapCreated: (GoogleMapController controller) {
                    _controller.complete(controller);
                  },
                  markers: <Marker>{
                    Marker(
                      draggable: true,
                      markerId: const MarkerId("1"),
                      position: currentLatLng,
                      icon: BitmapDescriptor.defaultMarker,
                      infoWindow: InfoWindow(
                        title: addressc,
                      ),
                    ),
                  },
                ),
              ),
              const SizedBox(height: 20),
              Row(
                children: [
                  const Icon(Icons.location_on),
                  AppTextSubTitle(
                    text: addressc,
                    color: AppColors.textColorDarkBlue,
                  ),
                  const Spacer(flex: 1),
                  InkWell(onTap: () {
                    addressc='';
                  },child: const AppTextTitle(text: 'Change',color: AppColors.textColorGray91,sizeText: 25,)),
                ],
              )
            ],
          ),
        ),
        Visibility(
          visible: addressc.isNotEmpty ? false : true,
          child: ButtomDottedBorder(
            assetIcon: AssetsPath.iconAddLocation,
            sizeHeight: 130,
            textContent: 'Location',
            textTitle:
                'Choose the location if you want to review it specifically',
            press: () {
              goToCurrentLocation();
            },
          ),
        ),
        const SizedBox(height: 10),
      ],
    );
  }
}
