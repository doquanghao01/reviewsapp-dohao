import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/widgets/app_text_sub_title.dart';
import '../../constants/assets_path.dart';
import 'widgets/body.dart';
import '../../widgets/app_header.dart';

class RateOrSharePage extends StatefulWidget {
  const RateOrSharePage({Key? key}) : super(key: key);

  @override
  State<RateOrSharePage> createState() => _RateOrSharePageState();
}

class _RateOrSharePageState extends State<RateOrSharePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AppHeader(
        textTitle: 'Rate & Share',
        isSubtitle: false,
        widget: InkWell(
          onTap: () {
            Navigator.maybePop(context);
          },
          child: const AppTextSubTitle(
            text: 'Cancel',
          ),
        ),
        isIcon: false,
        assetIcon: AssetsPath.iconMenu,
        bodyWidget: const Body(),
        press: () {},
      ),
    );
  }
}
