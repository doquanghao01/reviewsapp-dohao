import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/constants/assets_path.dart';
import 'package:reviewsapp_haodo/modules/rateorshare/rate_or_share_page.dart';
import '../widgets/app_icon.dart';
import 'navipages/notifications/notifications_page.dart';
import 'navipages/profile/profile_page.dart';
import 'navipages/reviews/reviews_page.dart';
import 'navipages/search/search_page.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  late int currentIndex = 0;
  var listIcon = {
    AssetsPath.iconReviews: 'Reviews',
    AssetsPath.iconSearch: 'Search',
    AssetsPath.iconPen: '',
    AssetsPath.iconNotifications: 'Notifications',
    AssetsPath.iconProfile: 'Profile',
  };
  List pages = [
    const ReviewsPage(),
    const SearchPage(),
    Container(),
    const NotificationsPage(),
    const ProfilePage()
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          pages[currentIndex],
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 100,
              decoration: const BoxDecoration(
                color: AppColors.kButtomNavBarColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
              ),
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: List.generate(
                  listIcon.length,
                  (index) => index != 2
                      ? onTapButtomNav(
                          index,
                          listIcon.keys.elementAt(index),
                          listIcon.values.elementAt(index),
                        )
                      : ButtomNavPen(assetsImg: listIcon.keys.elementAt(index)),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  InkWell onTapButtomNav(int index, String assetsIcon, String text) {
    return InkWell(
      onTap: () {
        setState(() {
          currentIndex = index;
        });
      },
      child: SizedBox(
        height: 50,
        width: 75,
        child: AppIcon(
          isPress: currentIndex == index ? true : false,
          assetIcon: assetsIcon,
          text: text,
        ),
      ),
    );
  }
}

class ButtomNavPen extends StatelessWidget {
  const ButtomNavPen({
    Key? key,
    required this.assetsImg,
  }) : super(key: key);
  final String assetsImg;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => const RateOrSharePage(),
          ),
        );
      },
      child: Container(
        height: 50,
        width: 50,
        padding: const EdgeInsets.all(13),
        margin: const EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100),
            color: AppColors.iconBlue),
        child: SvgPicture.asset(
          assetsImg,
          color: Colors.white,
        ),
      ),
    );
  }
}
