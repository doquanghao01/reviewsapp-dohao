import 'dart:async';

import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/constants/assets_path.dart';
import 'package:reviewsapp_haodo/modules/signinorsingup/sign_in_page.dart';
import 'package:reviewsapp_haodo/widgets/app_animated_logo.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  double size = 0;
  @override
  void initState() {
    super.initState();
    update();
  }

  void update() {
    Timer(
      const Duration(milliseconds: 200),
      () {
        setState(() {
          size = 200;
        });
      },
    );
    Timer(
      const Duration(milliseconds: 2300),
      () {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => const SignInPage(),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage(AssetsPath.imgWelcome),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          children: [
            const Spacer(flex: 2),
            AppAnimatedLogo(size: size),
            const Spacer(flex: 2),
          ],
        ),
      ),
    );
  }
}
