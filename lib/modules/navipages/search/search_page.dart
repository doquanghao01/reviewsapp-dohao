import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/widgets/app_text_field_custom.dart';

import 'widgets/body.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.kPrimaryColor,
      child: Column(
        children: const [
          Padding(
            padding: EdgeInsets.all(20.0),
            child: AppTextFieldCustom(
              hintText: 'Search review',
              icon: Icons.search,
              isIcon: true,
            ),
          ),
          Body(),
        ],
      ),
    );
  }
}
