
import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/constants/assets_path.dart';
import 'package:reviewsapp_haodo/widgets/app_text_sub_title.dart';
import 'package:reviewsapp_haodo/widgets/app_text_title.dart';

class BoxHistory extends StatelessWidget {
  const BoxHistory({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 20),
      child: Row(
        children: [
          Container(
            height: 74,
            width: 74,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              image: const DecorationImage(
                  image: AssetImage(AssetsPath.imgFood1),
                  fit: BoxFit.cover),
            ),
          ),
          const SizedBox(width: 10),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                AppTextTitle(
                  text: 'Secangkir jawa',
                  sizeText: 20,
                  color: AppColors.textColorDarkBlue,
                ),
                AppTextSubTitle(
                  text:
                      'I\'m very happy with the services. I think this is the best cafe in Yogyakar',
                  sizeText: 18,
                  color: AppColors.textColorDarkBlue,
                  isMaxLines: true,
                  isOverflow: true,
                  maxLines: 2,
                ),
                AppTextSubTitle(text: '6 minutes ago')
              ],
            ),
          ),
        ],
      ),
    );
  }
}
