import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/widgets/app_text_title.dart';

import 'box_history.dart';

class Body extends StatelessWidget {
  const Body({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        padding: const EdgeInsets.only(right: 20, left: 20),
        decoration: const BoxDecoration(
          color: AppColors.cardWhite,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 20),
              const AppTextTitle(
                text: 'History see',
                color: AppColors.textColorDarkBlue,
              ),
              const SizedBox(height: 20),
              ListView.builder(
                itemCount: 10,
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                primary: false,
                itemBuilder: (context, index) => const BoxHistory(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
