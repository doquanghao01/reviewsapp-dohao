import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import '../../../constants/assets_path.dart';
import '../../../widgets/app_header.dart';
import 'widgets/body.dart';

class NotificationsPage extends StatefulWidget {
  const NotificationsPage({Key? key}) : super(key: key);

  @override
  State<NotificationsPage> createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  @override
  Widget build(BuildContext context) {
    return AppHeader(
      textTitle: 'Notifications',
      isSubtitle: false,
      widget: Container(),
      assetIcon: AssetsPath.iconDotshHorizontal,
      bodyWidget: const BodyNotifications(),
      press: () {},
    );
  }
}

class BodyNotifications extends StatelessWidget {
  const BodyNotifications({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        decoration: const BoxDecoration(
          color: AppColors.cardLightGray,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: const Body(),
      ),
    );
  }
}
