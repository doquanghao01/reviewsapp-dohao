import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/constants/assets_path.dart';
import 'package:reviewsapp_haodo/widgets/app_circle_avatar.dart';
import 'package:reviewsapp_haodo/widgets/app_text_sub_title.dart';
import 'package:reviewsapp_haodo/widgets/app_text_title.dart';

class Status extends StatelessWidget {
  final int status;
  final String textName;
  final String textNoti;
  final String textTime;

  const Status(
      {Key? key,
      required this.status,
      required this.textName,
      required this.textNoti,
      required this.textTime})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    String getStatus(int status) {
      switch (status) {
        case 0:
          return AssetsPath.iconTUpHS;
        case 1:
          return AssetsPath.iconTDownHS;
        case 2:
          return AssetsPath.iconChat;
        case 3:
          return AssetsPath.iconRedo;
        default:
          return AssetsPath.iconRedo;
      }
    }

    Color getColor(int status) {
      switch (status) {
        case 0:
          return Colors.blue;
        case 1:
          return Colors.black45;

        case 2:
          return const Color.fromRGBO(68, 54, 157, 1);
        case 3:
          return Colors.green;
        default:
          return Colors.white;
      }
    }

    return Row(
      children: [
        Stack(
          children: [
            Container(
              padding: const EdgeInsets.all(10),
              child: AppCircleAvatar(img: AssetsPath.imgAvater),
            ),
            Visibility(
              visible: status != 4 ? true : false,
              child: Positioned(
                right: 0,
                bottom: 2,
                child: Container(
                  height: 30,
                  width: 30,
                  padding: const EdgeInsets.all(6),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(100),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset:
                            const Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  child: SvgPicture.asset(
                    getStatus(status),
                    color: getColor(status),
                  ),
                ),
              ),
            ),
          ],
        ),
        const SizedBox(width: 20),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text.rich(
                TextSpan(
                  children: [
                    WidgetSpan(
                      child: Container(
                        padding: const EdgeInsets.only(right: 5),
                        child: AppTextTitle(
                          text: textName,
                          color: AppColors.textColorDarkBlue,
                          sizeText: 20,
                        ),
                      ),
                    ),
                    TextSpan(
                        text: textNoti, style: const TextStyle(fontSize: 16)),
                  ],
                ),
              ),
              AppTextSubTitle(
                text: textTime,
                sizeText: 18,
              )
            ],
          ),
        ),
      ],
    );
  }
}
