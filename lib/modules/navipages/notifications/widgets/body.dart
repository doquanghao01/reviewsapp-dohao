import 'package:flutter/material.dart';
import 'notifications_card.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Container(
        padding: const EdgeInsets.only(right: 20, left: 20, bottom: 100),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const [
            NotificationsCard(
              text: 'JUST NOW',
              list: [0, 4],
            ),
            NotificationsCard(
              text: 'YESTERDAY',
              list: [0, 4, 2, 3],
            ),
            NotificationsCard(
              text: 'THIS WEEK',
              list: [0, 4, 2, 3, 1, 2],
            ),
          ],
        ),
      ),
    );
  }
}
