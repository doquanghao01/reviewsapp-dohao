import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/widgets/app_text_title.dart';

import 'status.dart';

class NotificationsCard extends StatelessWidget {
  const NotificationsCard({
    Key? key,
    required this.text,
    required this.list,
  }) : super(key: key);
  final String text;
  final List list;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AppTextTitle(
            text: text,
            sizeText: 25,
            color: AppColors.textColorGray91,
          ),
          ListView.builder(
            padding: EdgeInsets.zero,
            shrinkWrap: true,
            primary: false,
            itemCount: list.length,
            itemBuilder: (context, index) => Status(
              status: list[index],
              textName: 'Loretta Fleming',
              textNoti: 'liked your reviw',
              textTime: 'Just now',
            ),
          ),
          const SizedBox(height: 15),
          Container(
            height: 1,
            width: double.maxFinite,
            color: Colors.black.withOpacity(0.2),
          ),
        ],
      ),
    );
  }
}
