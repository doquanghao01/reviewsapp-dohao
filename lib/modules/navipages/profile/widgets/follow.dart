import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/widgets/app_text_title.dart';

class Follow extends StatelessWidget {
  const Follow({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            const ColumnFollow(textNumber: '386', text: 'Followers'),
            Container(
              width: 1,
              height: 50,
              color: AppColors.textColorDarkBlue,
            ),
            const ColumnFollow(textNumber: '364', text: 'Following'),
          ],
        ),
        const SizedBox(height: 30),
      ],
    );
  }
}

class ColumnFollow extends StatelessWidget {
  const ColumnFollow({
    Key? key,
    required this.textNumber,
    required this.text,
  }) : super(key: key);
  final String textNumber;
  final String text;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        AppTextTitle(
          text: textNumber,
          color: AppColors.textColorDarkBlue,
        ),
        const SizedBox(height: 10),
        AppTextTitle(
          color: AppColors.textColorGray91,
          text: text,
        )
      ],
    );
  }
}
