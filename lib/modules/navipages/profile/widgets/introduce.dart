import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/widgets/app_text_sub_title.dart';
import 'package:reviewsapp_haodo/widgets/app_text_title.dart';

class Introduce extends StatelessWidget {
  const Introduce({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 40, right: 40, top: 20),
      child: Column(
        children: const [
          AppTextTitle(
            text: 'Carolyne Agustine',
            color: AppColors.textColorDarkBlue,
          ),
          SizedBox(height: 20),
          AppTextSubTitle(
            text: 'Hey there! I\'m using this app for rate & review anything!',
            isTextAlign: true,
          ),
        ],
      ),
    );
  }
}
