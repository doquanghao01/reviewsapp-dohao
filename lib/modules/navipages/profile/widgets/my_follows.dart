import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/constants/assets_path.dart';
import 'package:reviewsapp_haodo/widgets/app_circle_avatar.dart';
import 'package:reviewsapp_haodo/widgets/app_text_sub_title.dart';

import 'content.dart';

class Follows extends StatelessWidget {
  const Follows({
    Key? key,
    required this.amount,
    required this.title,
  }) : super(key: key);
  final String amount;
  final String title;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Content(
          amount: amount,
          title: title,
        ),
        Container(
          margin: const EdgeInsets.only(right: 20, left: 20),
          padding:
              const EdgeInsets.only(top: 20, right: 20, left: 20, bottom: 20),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: List.generate(
              4,
              (index) => Column(
                children: [
                  AppCircleAvatar(
                    img: AssetsPath.imgAvater,
                  ),
                  const SizedBox(height: 10),
                  const AppTextSubTitle(
                    text: 'Alexander',
                    color: AppColors.textColorDarkBlue,
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
