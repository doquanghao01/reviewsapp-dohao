import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/constants/assets_path.dart';
import 'package:reviewsapp_haodo/widgets/app_circle_avatar.dart';

class Header extends StatelessWidget {
  const Header({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: Container(
        width: double.maxFinite,
        height: 280,
        decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage(AssetsPath.img3), fit: BoxFit.cover),
        ),
        child: Center(
          child: CircleAvatar(
            backgroundColor: Colors.white,
            radius: 30,
            child: AppCircleAvatar(
              img: AssetsPath.imgAvater,
            ),
          ),
        ),
      ),
    );
  }
}
