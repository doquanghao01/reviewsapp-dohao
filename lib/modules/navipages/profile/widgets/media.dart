import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/constants/assets_path.dart';

import 'content.dart';

class Media extends StatelessWidget {
  const Media({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Content(
          isamount: false,
          title: 'Media',
        ),
        Container(
          margin: const EdgeInsets.only(right: 20, left: 20),
          padding: const EdgeInsets.all(20),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: List.generate(
              3,
              (index) => Container(
                width: 100,
                height: 100,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: const DecorationImage(
                      image: AssetImage(AssetsPath.imgFood2),
                      fit: BoxFit.cover),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
