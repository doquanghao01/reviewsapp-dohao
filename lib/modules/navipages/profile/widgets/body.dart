import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/modules/navipages/reviews/detail/detail_page.dart';
import 'package:reviewsapp_haodo/widgets/app_post.dart';

import 'content.dart';
import 'follow.dart';
import 'introduce.dart';
import 'media.dart';
import 'my_follows.dart';

class Body extends StatelessWidget {
  const Body({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 80,
      left: 0,
      right: 0,
      top: 200,
      child: Container(
        decoration: const BoxDecoration(
          color: AppColors.cardWhite,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              const Introduce(),
              const Follow(),
              Container(
                color: AppColors.cardLightGray,
                child: Column(
                  children: const [
                    Content(
                      amount: '683',
                      title: 'My Reviews',
                    ),
                    AppPosts(widget: DetailPage(), index: 1),
                    Follows(
                      title: 'My Followers',
                      amount: '382',
                    ),
                    Follows(
                      title: 'My Following',
                      amount: '382',
                    ),
                    SizedBox(height: 20),
                    Media(),
                    SizedBox(height: 40),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
