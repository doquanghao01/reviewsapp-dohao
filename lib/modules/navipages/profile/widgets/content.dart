import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/widgets/app_text_sub_title.dart';
import 'package:reviewsapp_haodo/widgets/app_text_title.dart';

class Content extends StatelessWidget {
  const Content({
    Key? key,
    required this.title,
    this.amount = '',
    this.isamount = true,
  }) : super(key: key);
  final String title;
  final String amount;
  final bool isamount;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 20, right: 20, left: 20, bottom: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: Row(
              children: [
                AppTextTitle(
                  text: title,
                  sizeText: 25,
                  color: AppColors.textColorDarkBlue,
                ),
                const SizedBox(width: 10),
                Visibility(
                  visible: isamount,
                  child: AppTextSubTitle(
                    text: '($amount)',
                    sizeText: 25,
                  ),
                ),
              ],
            ),
          ),
          const AppTextSubTitle(
            text: 'See All',
            color: AppColors.textColorpurple54,
          ),
        ],
      ),
    );
  }
}
