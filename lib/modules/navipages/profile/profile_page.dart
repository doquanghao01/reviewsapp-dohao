import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:reviewsapp_haodo/constants/assets_path.dart';

import 'widgets/body.dart';
import 'widgets/header.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.maxFinite,
      height: double.maxFinite,
      child: Stack(
        children: [
          const Header(),
          Positioned(
            right: 20,
            top: 60,
            child: SizedBox(
              width: 24,
              height: 24,
              child: SvgPicture.asset(
                AssetsPath.iconPen,
                color: Colors.white,
              ),
            ),
          ),
          const Body()
        ],
      ),
    );
  }
}
