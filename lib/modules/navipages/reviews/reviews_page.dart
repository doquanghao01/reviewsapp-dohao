import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../../constants/assets_path.dart';
import '../../../widgets/app_header.dart';
import '../../../widgets/app_post.dart';
import 'detail/detail_page.dart';

class ReviewsPage extends StatefulWidget {
  const ReviewsPage({Key? key}) : super(key: key);

  @override
  State<ReviewsPage> createState() => _ReviewsPageState();
}

class _ReviewsPageState extends State<ReviewsPage> {
  @override
  Widget build(BuildContext context) {
    return AppHeader(
      textTitle: 'Reviews',
      textSubTitle: 'Browse any reviews for your reference',
      widget: Container(),
      assetIcon: AssetsPath.iconMenu,
      bodyWidget: const ListPosts(),
      press: () {},
    );
  }
}

class ListPosts extends StatelessWidget {
  const ListPosts({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(bottom: 100),
        child: ListView.builder(
          padding: EdgeInsets.zero,
          itemCount: 5,
          itemBuilder: (context, index) =>
              AppPosts(widget: const DetailPage(), index: index),
        ),
      ),
    );
  }
}
