import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/constants/assets_path.dart';
import 'package:reviewsapp_haodo/widgets/app_header.dart';

import 'widgets/body.dart';
import 'widgets/bottom_nav_bar.dart';

class DetailPage extends StatelessWidget {
  const DetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AppHeader(
        textTitle: 'Carolyne Agustine',
        textTitleSize: 25,
        textSubTitle: '26 minutes ago',
        isIconBack: true,
        assetIcon: AssetsPath.iconDotshHorizontal,
        bodyWidget: const Body(),
        press: () {
          Navigator.maybePop(context);
        },
        widget: Container(),
      ),
      bottomNavigationBar: const BottomNavBar(),
    );
  }
}
