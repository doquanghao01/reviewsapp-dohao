import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';

class BottomNavBar extends StatelessWidget {
  const BottomNavBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: 85,
      padding: const EdgeInsets.only(top: 10, left: 15, right: 15, bottom: 25),
      color: Colors.white,
      child: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.only(left: 20),
        width: MediaQuery.of(context).size.width * 1,
        decoration: BoxDecoration(
          color: const Color.fromARGB(255, 190, 190, 190).withOpacity(0.1),
          borderRadius: BorderRadius.circular(30),
        ),
        child: Stack(
          children: [
            const TextField(
              style: TextStyle(fontSize: 20),
              decoration: InputDecoration(
                hintText: "Write comment here...",
                hintStyle: TextStyle(
                  fontSize: 20.0,
                  color: AppColors.textHintColorGray91,
                ),
                border: InputBorder.none,
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Container(
                height: 40,
                width: 40,
                padding: const EdgeInsets.all(13),
                margin: const EdgeInsets.only(right: 4),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                    color: AppColors.kPrimaryColor),
                child: SvgPicture.asset(
                  'assets/icons/send.svg',
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
