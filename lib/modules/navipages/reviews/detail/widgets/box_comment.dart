import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/constants/assets_path.dart';
import 'package:reviewsapp_haodo/widgets/app_circle_avatar.dart';
import 'package:reviewsapp_haodo/widgets/app_text_sub_title.dart';
import 'package:reviewsapp_haodo/widgets/app_text_title.dart';

class BoxComment extends StatelessWidget {
  const BoxComment({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const AppTextTitle(
          text: 'Douglas Schnei',
          color: AppColors.textColorDarkBlue,
          sizeText: 18,
        ),
        const SizedBox(height: 10),
        const AppTextSubTitle(
          text: 'Thanks for the information. Your review help me very much :D',
          color: AppColors.textColorDarkBlue,
          sizeText: 16,
        ),
        const SizedBox(height: 10),
        Row(
          children: const [
            AppTextSubTitle(
              text: '3m ago',
              sizeText: 16,
            ),
            SizedBox(
              width: 5,
            ),
            AppTextTitle(
              text: 'Like',
              color: AppColors.kIconOffColor,
              sizeText: 18,
            ),
            SizedBox(
              width: 5,
            ),
            AppTextTitle(
              text: 'Relpy',
              color: AppColors.kIconOffColor,
              sizeText: 18,
            ),
          ],
        ),
        const SizedBox(height: 10),
        Row(
          children: [
            AppCircleAvatar(
              radius: 18,
              img: AssetsPath.imgAvater,
            ),
            const SizedBox(width: 10),
            const AppTextTitle(
              text: 'Eleanor Waters',
              sizeText: 18,
              color: AppColors.textColorDarkBlue,
            ),
            const SizedBox(width: 10),
            const Flexible(
              child: AppTextSubTitle(
                text: 'I aggreed with you',
                isOverflow: true,
                maxLines: 1,
                isMaxLines: true,
                sizeText: 16,
                color: AppColors.textColorDarkBlue,
              ),
            ),
          ],
        )
      ],
    );
  }
}
