import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/constants/assets_path.dart';
import 'package:reviewsapp_haodo/widgets/app_circle_avatar.dart';

import 'box_comment.dart';

class ListComment extends StatelessWidget {
  const ListComment({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      itemCount: 5,
      itemBuilder: (context, index) => Container(
        alignment: Alignment.topLeft,
        padding: const EdgeInsets.only(top: 20, left: 15, right: 15),
        color: AppColors.cardLightGray,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AppCircleAvatar(
              img: AssetsPath.imgAvater,
            ),
            Flexible(
              child: Container(
                margin: const EdgeInsets.only(top: 5, left: 10, bottom: 10),
                padding: const EdgeInsets.all(20),
                decoration: const BoxDecoration(
                  color: AppColors.cardWhite,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                    bottomLeft: Radius.circular(10),
                  ),
                ),
                child: const BoxComment(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
