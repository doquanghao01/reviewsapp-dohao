import 'dart:async';

import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';

import 'widgets/body_sign_up.dart';
import 'widgets/header.dart';

class SignUpPase extends StatefulWidget {
  const SignUpPase({Key? key}) : super(key: key);

  @override
  State<SignUpPase> createState() => _SignUpPaseState();
}

class _SignUpPaseState extends State<SignUpPase> {
  double size = 0;
  @override
  void initState() {
    super.initState();
    update();
  }

  void update() {
    Timer(
      const Duration(milliseconds: 200),
      () {
        setState(() {
          size = 150;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: AppColors.cardLightGray,
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Header(
                size: size,
                text: 'Sign Up',
              ),
              const Expanded(child: BodySignUp()),
            ],
          ),
        ),
      ),
    );
  }
}
