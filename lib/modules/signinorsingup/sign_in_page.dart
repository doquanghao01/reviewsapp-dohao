import 'dart:async';

import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';

import 'widgets/body_sign_in.dart';
import 'widgets/header.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  double size = 0;
  @override
  void initState() {
    super.initState();
    update();
  }

  void update() {
    Timer(
      const Duration(milliseconds: 200),
      () {
        setState(() {
          size = 150;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: AppColors.cardLightGray,
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Header(
                size: size,
                text: 'Sign In',
              ),
              const Expanded(child: BodySignIn()),
            ],
          ),
        ),
      ),
    );
  }
}
