
import 'package:flutter/material.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/widgets/app_animated_logo.dart';
import 'package:reviewsapp_haodo/widgets/app_text_title.dart';

class Header extends StatelessWidget {
  const Header({
    Key? key,
    required this.size, required this.text,
  }) : super(key: key);

  final double size;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: 250,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.only(
          bottomRight: Radius.circular(30),
          bottomLeft: Radius.circular(30),
        ),
        color: Colors.white,
      ),
      child: Column(
        children: [
          const Spacer(flex: 2),
          AppAnimatedLogo(size: size),
          const Spacer(flex: 1),
           AppTextTitle(
            text: text,
            color: AppColors.textColorDarkBlue,
          ),
          const Spacer(flex: 1),
        ],
      ),
    );
  }
}
