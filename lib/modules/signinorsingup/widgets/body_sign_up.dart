import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/constants/assets_path.dart';
import 'package:reviewsapp_haodo/modules/signinorsingup/sign_in_page.dart';
import 'package:reviewsapp_haodo/widgets/app_bottom.dart';
import 'package:reviewsapp_haodo/widgets/app_text_field_custom.dart';
import 'package:reviewsapp_haodo/widgets/app_text_sub_title.dart';
import 'package:reviewsapp_haodo/widgets/app_text_title.dart';

class BodySignUp extends StatelessWidget {
  const BodySignUp({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const AppTextFieldCustom(hintText: 'Email'),
            const AppTextFieldCustom(hintText: 'Full Name'),
            const AppTextFieldCustom(
              hintText: 'Password',
              isObscureText: true,
            ),
            const AppTextFieldCustom(
              hintText: 'Comfirm Password',
              isObscureText: true,
            ),
            const SizedBox(height: 20),
            const AppBottom(
              text: 'Sign-Up',
              colorButtom: AppColors.kPrimaryColor,
              radius: 20,
            ),
            const SizedBox(height: 50),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const AppTextSubTitle(
                  text: 'Already a Member?  ',
                  color: AppColors.textColorDarkBlue,
                ),
                InkWell(
                  onTap: () {
                    Navigator.maybePop(context);
                  },
                  child: const AppTextTitle(
                    text: 'Sign In',
                    color: AppColors.kPrimaryColor,
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10),
            const Center(
              child: AppTextSubTitle(
                text: 'Or',
                color: AppColors.kPrimaryColor,
              ),
            ),
            const SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 32,
                  height: 32,
                  child: SvgPicture.asset(AssetsPath.iconGoogle),
                ),
                const SizedBox(width: 20),
                SizedBox(
                  width: 32,
                  height: 32,
                  child: SvgPicture.asset(AssetsPath.iconFacebook),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
