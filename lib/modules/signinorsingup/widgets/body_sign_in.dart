import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:reviewsapp_haodo/config/themes/app_color.dart';
import 'package:reviewsapp_haodo/constants/assets_path.dart';
import 'package:reviewsapp_haodo/modules/main_page.dart';
import 'package:reviewsapp_haodo/modules/signinorsingup/sign_up_page.dart';
import 'package:reviewsapp_haodo/widgets/app_bottom.dart';
import 'package:reviewsapp_haodo/widgets/app_text_field_custom.dart';
import 'package:reviewsapp_haodo/widgets/app_text_sub_title.dart';
import 'package:reviewsapp_haodo/widgets/app_text_title.dart';

class BodySignIn extends StatelessWidget {
  const BodySignIn({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const AppTextFieldCustom(hintText: 'Email'),
            const AppTextFieldCustom(
              hintText: 'Password',
              isObscureText: true,
            ),
            const SizedBox(height: 20),
            const AppTextSubTitle(
              text: 'Forgot password?',
              color: AppColors.kPrimaryColor,
            ),
            const SizedBox(height: 20),
            InkWell(
              onTap: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const MainPage(),
                  ),
                );
                
              },
              child: const AppBottom(
                text: 'Login',
                colorButtom: AppColors.kPrimaryColor,
                radius: 20,
              ),
            ),
            const SizedBox(height: 120),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const AppTextSubTitle(
                  text: 'Do not have an account!  ',
                  color: AppColors.textColorDarkBlue,
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const SignUpPase(),
                      ),
                    );
                  },
                  child: const AppTextTitle(
                    text: 'Sign Up',
                    color: AppColors.kPrimaryColor,
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10),
            const Center(
              child: AppTextSubTitle(
                text: 'Or',
                color: AppColors.kPrimaryColor,
              ),
            ),
            const SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 32,
                  height: 32,
                  child: SvgPicture.asset(AssetsPath.iconGoogle),
                ),
                const SizedBox(width: 20),
                SizedBox(
                  width: 32,
                  height: 32,
                  child: SvgPicture.asset(AssetsPath.iconFacebook),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
